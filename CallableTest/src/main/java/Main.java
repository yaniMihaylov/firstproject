import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        File file = new File("/home/qni/test");

        Callable<CallableResult> demo = new CallableDemo(file, "^[a]\\w*.*");

        ExecutorService service = Executors.newSingleThreadExecutor();


        Future<CallableResult> submit = service.submit(demo);

        System.out.println(submit.get().returnNames());
        service.shutdown();
    }
}
