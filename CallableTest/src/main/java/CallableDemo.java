import java.io.File;
import java.util.concurrent.Callable;

public class CallableDemo implements Callable<CallableResult> {
    private File directory;
    private CallableResult callableResult;
    private String regex;

    public CallableDemo(File directory, String regex) {
        this.directory = directory;
        this.regex = regex;
        this.callableResult = new CallableResult(regex);
    }

    public CallableResult call() throws Exception {

        File[] files = directory.listFiles();

        if (files == null) {
            throw new NullPointerException();
        }

        for (File file : files) {
            if (file.getName().matches(regex)) {
                this.callableResult.addFiles(file);
            }
        }


        return callableResult;
    }
}
