import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CallableResult {
    private List<File> files;
    private String patter;

    public CallableResult(String patter) {
        this.files = new ArrayList<File>();
        this.patter = patter;
    }

    public List<File> getFiles() {
        return files;
    }

    public String getPatter() {
        return patter;
    }

    public void addFiles(File file){
        this.files.add(file);
    }

    public String returnNames(){
        StringBuilder sb = new StringBuilder();

        sb.append(patter).append(System.lineSeparator());

        for (File file : files) {
            sb.append(file.getName()).append(System.lineSeparator());
        }

        return sb.toString();
    }


}
